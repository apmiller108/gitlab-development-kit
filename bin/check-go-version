#!/usr/bin/env ruby

def version_to_a(v)
  v.split('.').map(&:to_i)
end

MINIMAL_VERSION = "1.12"

version_output = ""

begin
  version_output = `go version`
rescue Errno::ENOENT
  abort "Go is not installed, please install Go #{MINIMAL_VERSION} or higher"
end

abort 'Could not determine version of Go' unless $?.success?

expected = version_to_a(MINIMAL_VERSION)
actual = version_to_a(version_output[/go(\d+\.\d+)/, 1])

case actual <=> expected
when -1
  abort "Please install Go version #{MINIMAL_VERSION} or higher"
when nil
  abort "Unable to determine version of Go"
end
